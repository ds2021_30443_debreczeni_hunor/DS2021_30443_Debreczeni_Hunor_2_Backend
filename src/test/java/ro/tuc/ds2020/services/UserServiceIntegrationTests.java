package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.UserDTO;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

import java.util.List;
import java.util.Optional;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "test-sql/delete.sql")
public class UserServiceIntegrationTests extends Ds2020TestConfig {

    @Autowired
    UserService userService;

    @Test
    public void testGetCorrect() {
        List<UserDTO> personDTOList = userService.findAll();
        assertEquals("Test Insert Person", 1, personDTOList.size());
    }

    @Test
    public void testInsertCorrectWithGetById() {
        UserDTO p = new UserDTO(null, "email", "name",null,null, null, null);
        UserDTO insertedUser = userService.insert(p);

        UserDTO insertedPersonCopy = new UserDTO(insertedUser.getId(), p.getEmail(), p.getName(), null,null,null, null);
        Optional<UserDTO> fetchedPerson = userService.findById(insertedUser.getId());

        assertTrue("User is present", fetchedPerson.isPresent());
        assertEquals("Test Inserted Person", insertedPersonCopy, fetchedPerson.get());
    }

    @Test
    public void testInsertCorrectWithGetAll() {
        UserDTO p = new UserDTO(null, "Email", "Name",null,null, null, null);
        userService.insert(p);

        List<UserDTO> personDTOList = userService.findAll();
        assertEquals("Test Inserted Persons", 2, personDTOList.size());
    }
}
