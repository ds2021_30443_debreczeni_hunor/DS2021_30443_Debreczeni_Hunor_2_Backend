package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.SensorDTO;
import ro.tuc.ds2020.dtos.builders.SensorBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.SensorService;
import ro.tuc.ds2020.services.UserService;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/sensor")
public class SensorController extends CrudController<SensorDTO, Sensor> {

    private final SensorService service;
    private final UserService userService;
    private final SensorBuilder sensorBuilder;

    @Autowired
    SensorController(SensorService service, UserService userService, SensorBuilder sensorBuilder) {
        super(service);
        this.service = service;
        this.userService = userService;
        this.sensorBuilder = sensorBuilder;
    }

    @Override
    protected String getTypeName() {
        return Sensor.class.getSimpleName();
    }

    @Override
    public ResponseEntity<List<SensorDTO>> readAll(Principal principal) {
        final User user = userService.findByEmail(principal.getName()).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        if (user.getPrivileges().contains("ROLE_ADMIN")) {
            return super.readAll(principal);
        }

        final Set<Device> devices = user.getDevices();
        final List<SensorDTO> sensorDTOs = devices.stream().map(Device::getSensors)
                .flatMap(Collection::stream)
                .map(sensorBuilder::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(sensorDTOs, HttpStatus.OK);
    }
}
