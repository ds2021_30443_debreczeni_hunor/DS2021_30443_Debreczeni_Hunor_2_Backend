package ro.tuc.ds2020.controllers;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MonitoredDataDTO;
import ro.tuc.ds2020.dtos.builders.MonitoredDataBuilder;
import ro.tuc.ds2020.entities.MonitoredData;
import ro.tuc.ds2020.services.MonitorService;

import java.util.Comparator;
import java.util.Optional;

@Component
public class RabbitMQController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQController.class);
    private static final Double MAX_PEAK = 0.0005D;

    @Autowired
    private MonitorService monitorService;
    @Autowired
    private MonitoredDataBuilder monitoredDataBuilder;
    @Autowired
    private SimpMessagingTemplate simp;

    @RabbitListener(queues = "queue")
    public void receivedMessage(String data) {
        MonitoredDataDTO monitoredDataDTO = new Gson().fromJson(data, MonitoredDataDTO.class);
        LOGGER.info("Received data from queue: " + monitoredDataDTO);

        MonitoredData monitoredData;
        try {
            monitoredData = monitoredDataBuilder.toEntity(monitoredDataDTO);
        } catch (ResourceNotFoundException ignored) {
            LOGGER.error("Sensor not found with id {}", monitoredDataDTO.getSensor_id());
            return;
        }

        final Double peak = getPeak(monitoredData);
        LOGGER.info("Peak is: {}", peak);
        monitoredData = monitorService.insert(monitoredData);

        if (peak > 0 && peak > MAX_PEAK) {
            LOGGER.info("Over threshold peak, sending notification...");
            String username = monitoredData.getSensor().getDevice().getUser().getEmail();
            simp.convertAndSendToUser(username, "/queue/notification", monitoredDataBuilder.toDto(monitoredData));
        }
    }

    private Double getPeak(MonitoredData monitoredData) {
        Optional<MonitoredData> lastMonitoredData = monitoredData.getSensor()
                .getDataSet()
                .stream()
                .max(Comparator.comparing(MonitoredData::getTimestamp));

        if (lastMonitoredData.isEmpty()) {
            return 0D;
        }
        final MonitoredData lastData = lastMonitoredData.get();
        return (monitoredData.getMeasurement_value() - lastData.getMeasurement_value()) / (monitoredData.getTimestamp() - lastData.getTimestamp());
    }
}
