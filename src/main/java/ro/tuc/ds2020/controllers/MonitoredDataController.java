package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MonitoredDataDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitoredData;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.MonitorService;
import ro.tuc.ds2020.services.UserService;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/data")
public class MonitoredDataController extends CrudController<MonitoredDataDTO, MonitoredData> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
    private final UserService userService;
    private final DeviceService deviceService;

    @Autowired
    public MonitoredDataController(MonitorService monitorService, DeviceService deviceService, UserService userService) {
        super(monitorService);
        this.userService = userService;
        this.deviceService = deviceService;
    }

    @Override
    protected String getTypeName() {
        return "Monitored data";
    }

    @GetMapping(value = "/daily-usage/{id}/{date}")
    public ResponseEntity<Map<Integer, Double>> dailyUsage(Principal principal, @PathVariable UUID id, @PathVariable String date) throws ParseException {
        //to check correctness
        DATE_FORMAT.parse(date);

        final Device device = deviceService.findObjById(id).orElseThrow(() -> new ResourceNotFoundException("Device not found"));
        if (!device.getUser().getEmail().equals(principal.getName())) {
            throw new ResourceNotFoundException("Device not found");
        }

        final Set<Sensor> sensors = device.getSensors();

        final Map<Integer, Double> dataSet = new HashMap<>();
        sensors.stream().map(Sensor::getDataSet)
                .flatMap(Collection::stream)
                .filter(monitoredData -> DATE_FORMAT.format(new Date(monitoredData.getTimestamp())).equals(date))
                .forEach(data -> dataSet.merge(new Date(data.getTimestamp()).getHours(), data.getMeasurement_value(), Double::sum));

        for (int i = 0; i < 24; i++) {
            dataSet.putIfAbsent(i, 0D);
        }

        return ResponseEntity.ok(dataSet);
    }
}
