package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.MonitoredData;
import ro.tuc.ds2020.entities.Sensor;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.DeviceService;
import ro.tuc.ds2020.services.UserService;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/device")
public class DeviceController extends CrudController<DeviceDTO, Device> {

    private final UserService userService;
    private final DeviceService deviceService;
    private final DeviceBuilder deviceBuilder;

    protected DeviceController(DeviceService service, DeviceService deviceService, UserService userService, DeviceBuilder deviceBuilder) {
        super(service);
        this.userService = userService;
        this.deviceService = deviceService;
        this.deviceBuilder = deviceBuilder;
    }

    @Override
    protected String getTypeName() {
        return Device.class.getSimpleName();
    }

    @Override
    public ResponseEntity<List<DeviceDTO>> readAll(Principal principal) {
        final User user = userService.findByEmail(principal.getName()).orElseThrow(() -> new ResourceNotFoundException("User not found"));
        if (user.getPrivileges().contains("ROLE_ADMIN")) {
            return super.readAll(principal);
        }

        final List<DeviceDTO> devices = user.getDevices().stream().map(deviceBuilder::toDto).collect(Collectors.toList());
        return new ResponseEntity<>(devices, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/usage")
    public ResponseEntity<Double> deviceUsage(Principal principal, @PathVariable UUID id) {
        final Device device = deviceService.findObjById(id).orElseThrow(() -> new ResourceNotFoundException("Device not found"));
        if (!device.getUser().getEmail().equals(principal.getName())) {
            throw new ResourceNotFoundException("Device not found");
        }

        final Double value = device.getSensors().stream()
                .map(Sensor::getDataSet)
                .flatMap(Collection::stream)
                .map(MonitoredData::getMeasurement_value)
                .reduce(0D, Double::sum);

        return ResponseEntity.ok(value);
    }
}
