package ro.tuc.ds2020.command;

import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.services.UserService;
import ro.tuc.ds2020.util.JwtTokenUtil;

import java.util.List;

@Component
public class AuthorizeTokenCommand {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizeTokenCommand.class);

    private final UserService userService;
    private final JwtTokenUtil jwtTokenUtil;

    @Autowired
    public AuthorizeTokenCommand(UserService userService, JwtTokenUtil jwtTokenUtil) {
        this.userService = userService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    public void apply(StompHeaderAccessor accessor) {
        LOGGER.info("Running command");

        final List<String> authorization = accessor.getNativeHeader("X-Authorization");

        final String jwtToken = authorization.get(0);
        String username = "";
        try {
            username = jwtTokenUtil.getUsernameFromToken(jwtToken);
        } catch (IllegalArgumentException e) {
            System.out.println("Unable to get JWT Token");
        } catch (ExpiredJwtException e) {
            System.out.println("JWT Token has expired");
        }

        UserDetails userDetails = userService.loadUserByUsername(username);

        // if token is valid configure Spring Security to manually set
        // authentication
        if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            // After setting the Authentication in the context, we specify
            // that the current user is authenticated. So it passes the
            // Spring Security Configurations successfully.
            accessor.setUser(usernamePasswordAuthenticationToken);
        } else {
            throw new RuntimeException("Invalid token");
        }
    }
}
