package ro.tuc.ds2020.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Sensor extends HasId {

    private String name;
    @ManyToOne
    @JoinColumn(name = "device_id", nullable = false)
    private Device device;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "sensor", cascade = CascadeType.REMOVE)
    private Set<MonitoredData> dataSet;

    public Sensor(UUID id, String name, Device device) {
        super(id);
        this.name = name;
        this.device = device;
    }
}
