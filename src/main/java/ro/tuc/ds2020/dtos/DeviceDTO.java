package ro.tuc.ds2020.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ro.tuc.ds2020.entities.HasId;

import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class DeviceDTO extends HasId {
    private String name;
    private UUID userId;
    private String userEmail;
    private Set<UUID> sensors;

    public DeviceDTO(UUID id, String name, UUID userId, String userEmail, Set<UUID> sensors) {
        super(id);
        this.name = name;
        this.userId = userId;
        this.userEmail = userEmail;
        this.sensors = sensors;
    }
}
