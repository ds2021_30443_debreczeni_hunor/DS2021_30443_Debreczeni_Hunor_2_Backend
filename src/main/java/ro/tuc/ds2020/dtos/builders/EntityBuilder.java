package ro.tuc.ds2020.dtos.builders;

import java.util.function.Consumer;
import java.util.function.Supplier;

public interface EntityBuilder<DTO, Entity> {
    DTO toDto(Entity entity);

    Entity toEntity(DTO dto);

    Entity toEntity(Entity original, DTO dto);

    default <T> void setIfPresent(Supplier<T> supplier, Consumer<T> consumer) {
        final T data = supplier.get();
        if (data != null) {
            consumer.accept(data);
        }
    }
}
