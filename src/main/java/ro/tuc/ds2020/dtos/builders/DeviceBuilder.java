package ro.tuc.ds2020.dtos.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.HasId;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.services.UserService;

import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public final class DeviceBuilder implements EntityBuilder<DeviceDTO, Device> {
    @Autowired
    private UserService userService;

    @Override
    public DeviceDTO toDto(Device device) {
        return new DeviceDTO(
                device.getId(),
                device.getName(),
                device.getUser().getId(),
                device.getUser().getEmail(),
                Optional.ofNullable(device.getSensors()).map(set ->
                        set.stream().map(HasId::getId).collect(Collectors.toSet())
                ).orElseGet(HashSet::new)
        );
    }

    @Override
    public Device toEntity(DeviceDTO deviceDTO) {
        final User user = userService.findObjById(deviceDTO.getUserId()).orElseThrow(() -> new IllegalArgumentException("User not found"));

        return new Device(
                deviceDTO.getUserId(),
                deviceDTO.getName(),
                user
        );
    }

    @Override
    public Device toEntity(Device original, DeviceDTO deviceDTO) {
        setIfPresent(deviceDTO::getName, original::setName);
        final User user = userService.findObjById(deviceDTO.getUserId()).orElseThrow(() -> new IllegalArgumentException("User not found"));
        original.setUser(user);

        return original;
    }
}
