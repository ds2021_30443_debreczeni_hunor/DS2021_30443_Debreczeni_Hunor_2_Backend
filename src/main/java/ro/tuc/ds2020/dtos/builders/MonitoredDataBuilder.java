package ro.tuc.ds2020.dtos.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MonitoredDataDTO;
import ro.tuc.ds2020.entities.MonitoredData;
import ro.tuc.ds2020.services.SensorService;

@Component
public final class MonitoredDataBuilder implements EntityBuilder<MonitoredDataDTO, MonitoredData> {
    @Autowired
    private SensorService sensorService;

    @Override
    public MonitoredDataDTO toDto(MonitoredData monitoredData) {
        return new MonitoredDataDTO(
                monitoredData.getTimestamp(),
                monitoredData.getSensor().getId(),
                monitoredData.getMeasurement_value()
        );
    }

    @Override
    public MonitoredData toEntity(MonitoredDataDTO monitoredDataDTO) {
        return MonitoredData.builder()
                .sensor(sensorService.findObjById(monitoredDataDTO.getSensor_id()).orElseThrow(() -> new ResourceNotFoundException("Sensor not found")))
                .timestamp(monitoredDataDTO.getTimestamp())
                .measurement_value(monitoredDataDTO.getMeasurement_value())
                .build();
    }

    @Override
    public MonitoredData toEntity(MonitoredData original, MonitoredDataDTO monitoredDataDTO) {
        setIfPresent(monitoredDataDTO::getTimestamp, original::setTimestamp);
        setIfPresent(monitoredDataDTO::getMeasurement_value, original::setMeasurement_value);

        return original;
    }
}
