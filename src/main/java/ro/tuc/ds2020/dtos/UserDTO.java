package ro.tuc.ds2020.dtos;

import lombok.*;
import ro.tuc.ds2020.entities.HasId;

import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class UserDTO extends HasId {
    private String email;
    private String name;
    private String password;
    private String address;
    private Integer age;
    private Set<UUID> devices;
    private Set<String> roles;

    public UserDTO(UUID id, String email, String name, String address, Integer age, Set<UUID> devices, Set<String> roles) {
        super(id);
        this.email = email;
        this.name = name;
        this.address = address;
        this.age = age;
        this.devices = devices;
        this.roles = roles;
    }
}
