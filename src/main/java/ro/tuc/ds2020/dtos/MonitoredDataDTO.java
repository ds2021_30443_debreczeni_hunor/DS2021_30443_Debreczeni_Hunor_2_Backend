package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ro.tuc.ds2020.entities.HasId;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonitoredDataDTO extends HasId {
    private Long timestamp;
    private UUID sensor_id;
    private Double measurement_value;
}
