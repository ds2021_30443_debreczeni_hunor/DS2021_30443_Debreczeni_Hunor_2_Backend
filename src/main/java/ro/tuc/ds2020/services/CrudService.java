package ro.tuc.ds2020.services;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.builders.EntityBuilder;
import ro.tuc.ds2020.entities.HasId;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class CrudService<DTO extends HasId, Obj> {
    private final EntityBuilder<DTO, Obj> entityBuilder;
    private final JpaRepository<Obj, UUID> repository;

    public CrudService(EntityBuilder<DTO, Obj> entityBuilder, JpaRepository<Obj, UUID> repository) {
        this.entityBuilder = entityBuilder;
        this.repository = repository;
    }

    public DTO insert(DTO object) {
        Obj obj = entityBuilder.toEntity(object);
        obj = repository.save(obj);
        return entityBuilder.toDto(obj);
    }

    public Obj insert(Obj object){
        return repository.save(object);
    }

    public List<DTO> findAll() {
        return repository.findAll().stream().map(entityBuilder::toDto).collect(Collectors.toList());
    }

    public Optional<DTO> findById(UUID id) {
        return findObjById(id).map(entityBuilder::toDto);
    }

    public Optional<Obj> findObjById(UUID id){
        return repository.findById(id);
    }

    public DTO update(DTO dto) {
        final Obj original = repository.findById(dto.getId()).orElseThrow(() -> new ResourceNotFoundException("Not found by id " + dto.getId()));
        return entityBuilder.toDto(repository.save(entityBuilder.toEntity(original, dto)));
    }

    public void deleteById(UUID id) {
        repository.deleteById(id);
    }

    protected void delete(DTO dto) {
        deleteById(dto.getId());
    }
}
