package ro.tuc.ds2020.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.entities.Privilege;
import ro.tuc.ds2020.entities.Role;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.PrivilegeRepository;
import ro.tuc.ds2020.repositories.RoleRepository;
import ro.tuc.ds2020.repositories.UserRepository;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String ROLE_USER = "ROLE_USER";
    private static final String ROLE_DEVICE_EDITOR = "ROLE_DEVICE_EDITOR";
    private static final String ROLE_SENSOR_EDITOR = "ROLE_SENSOR_EDITOR";

    /**
     * There's a hierarchy: Admin -> Client (Admin has every privilege which the client has)
     */
    private static final Map<String, Set<String>> DEFAULT_PRIVILEGES = Map.of(
            ROLE_USER, Set.of(
                    "READ_SENSOR",
                    "READ_DEVICE"
            ),
            ROLE_DEVICE_EDITOR, Set.of(
                    "CREATE_DEVICE",
                    "READ_DEVICE",
                    "UPDATE_DEVICE",
                    "DELETE_DEVICE"
            ),
            ROLE_SENSOR_EDITOR, Set.of(
                    "CREATE_SENSOR",
                    "READ_SENSOR",
                    "UPDATE_SENSOR",
                    "DELETE_SENSOR"
            ),
            ROLE_ADMIN, Set.of(
                    "CREATE_USER",
                    "READ_USER",
                    "UPDATE_USER",
                    "DELETE_USER"
            )
    );
    private boolean alreadySetup = false;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        Set<Role> allRoles = new HashSet<>();
        DEFAULT_PRIVILEGES.forEach((roleName, privileges) -> {
            final Set<Privilege> privilegeCollection = new HashSet<>();
            privileges.forEach(privilegeName -> privilegeCollection.add(createPrivilegeIfNotFound(privilegeName)));

            allRoles.add(createRoleIfNotFound(roleName, privilegeCollection));
        });

        createUserIfNotFound("admin", "test", allRoles);

        alreadySetup = true;
    }

    @Transactional
    Privilege createPrivilegeIfNotFound(final String name) {
        return privilegeRepository.findByName(name)
                .orElseGet(() -> privilegeRepository.save(new Privilege(name)));

    }

    @Transactional
    Role createRoleIfNotFound(final String name, final Set<Privilege> privileges) {
        final Role role = roleRepository.findByName(name).orElseGet(() -> new Role(name));
        role.setPrivileges(privileges);
        return roleRepository.save(role);
    }

    @Transactional
    User createUserIfNotFound(final String email, final String password, final Set<Role> roles) {
        Optional<User> userOptional = userRepository.findByEmail(email);
        final User user;
        if (userOptional.isPresent()) {
            user = userOptional.get();
        } else {
            user = new User();
            user.setEmail(email);
            user.setPassword(passwordEncoder.encode(password));
        }

        user.setRoles(roles);
        return userRepository.save(user);
    }
}
