package ro.tuc.ds2020.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ComponentScan(basePackages = {"ro.tuc.ds2020.config"})
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    private UserDetailsService jwtUserDetailsService;

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/resources/**")
                .antMatchers("/h2/**");
    }

    @Override
    protected void configure(final HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .cors().and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/user").hasAuthority("CREATE_USER")
                .antMatchers(HttpMethod.GET, "/api/user").hasAuthority("READ_USER")
                .antMatchers(HttpMethod.PUT, "/api/user").hasAuthority("UPDATE_USER")
                .antMatchers(HttpMethod.DELETE, "/api/user/*").hasAuthority("DELETE_USER")

                .antMatchers(HttpMethod.POST, "/api/device").hasAuthority("CREATE_DEVICE")
                .antMatchers(HttpMethod.GET, "/api/device").hasAuthority("READ_DEVICE")
                .antMatchers(HttpMethod.PUT, "/api/device").hasAuthority("UPDATE_DEVICE")
                .antMatchers(HttpMethod.DELETE, "/api/device/*").hasAuthority("DELETE_DEVICE")

                .antMatchers(HttpMethod.POST, "/api/sensor").hasAuthority("CREATE_SENSOR")
                .antMatchers(HttpMethod.GET, "/api/sensor").hasAuthority("READ_SENSOR")
                .antMatchers(HttpMethod.PUT, "/api/sensor").hasAuthority("UPDATE_SENSOR")
                .antMatchers(HttpMethod.DELETE, "/api/sensor/*").hasAuthority("DELETE_SENSOR")

                .antMatchers(HttpMethod.POST, "/api/auth/*").permitAll()
                .antMatchers(HttpMethod.GET, "/api/user/roleCheck").authenticated()
                .antMatchers(HttpMethod.GET, "/api/data/daily-usage/*/*").authenticated()
                .antMatchers(HttpMethod.GET, "/api/device/*/usage").authenticated()
                .antMatchers("/ws/*").permitAll() //Validated by WebSocketMessageInterceptor
                .antMatchers("/ws/**").permitAll() //Validated by WebSocketMessageInterceptor
                .antMatchers("/").permitAll()
                .anyRequest().denyAll()
                .and()
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint);

        httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);


    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

}
