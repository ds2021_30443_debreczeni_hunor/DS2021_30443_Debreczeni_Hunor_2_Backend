package ro.tuc.ds2020.config.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import ro.tuc.ds2020.command.AuthorizeTokenCommand;

import java.util.Objects;

@Configuration
public class WebSocketMessageInterceptor implements ChannelInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketMessageInterceptor.class);
    private final AuthorizeTokenCommand authorizeTokenCommand;

    @Autowired
    public WebSocketMessageInterceptor(AuthorizeTokenCommand authorizeTokenCommand) {
        this.authorizeTokenCommand = authorizeTokenCommand;
    }

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        final StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        if (Objects.requireNonNull(Objects.requireNonNull(accessor).getCommand()) == StompCommand.CONNECT) {
            authorizeTokenCommand.apply(accessor);
        }
        return message;
    }
}
