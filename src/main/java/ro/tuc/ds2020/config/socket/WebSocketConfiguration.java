package ro.tuc.ds2020.config.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

import java.security.Principal;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketMessageInterceptor.class);
    //    private final ClearSubscriptionsCommand clearSubscriptionsCommand;
//    private final UnsubscribeCommand unsubscribeCommand;
//    @Autowired
//    SimpMessagingTemplate simpMessagingTemplate;

//    @Autowired
//    public WebSocketConfiguration(ClearSubscriptionsCommand clearSubscriptionsCommand, UnsubscribeCommand unsubscribeCommand) {
//        this.clearSubscriptionsCommand = clearSubscriptionsCommand;
//        this.unsubscribeCommand = unsubscribeCommand;
//    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/notification", "/user", "/queue");
//        registry.setUserDestinationPrefix("/user");
//        registry.setApplicationDestinationPrefixes("/ws");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws/connect")
                .setAllowedOriginPatterns("*")
                .withSockJS();
    }

    @EventListener
    public void onSocketConnected(SessionConnectedEvent event) {
        LOGGER.info("[Connected] " + event.getUser().getName());
    }

    @EventListener
    public void onSubscribeEvent(SessionSubscribeEvent event){
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
//        subscribeCommand.apply(simpMessagingTemplate, event.getUser(), );
        System.out.println("[Subscribed] " + sha.getSessionId());
    }

    @EventListener
    public void onUnsubscribeEvent(SessionUnsubscribeEvent event) {
        LOGGER.info("UNSUBSCRIBE command");

        final StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());
//        final String[] subscriptionIdParts = Objects.requireNonNull(accessor.getSubscriptionId()).split("-");
//        unsubscribeCommand.apply(Objects.requireNonNull(accessor.getUser()).getName(), subscriptionIdParts[0], subscriptionIdParts[1]);
    }

    @EventListener
    public void onSocketDisconnected(SessionDisconnectEvent event) {
        LOGGER.info("DISCONNECT command");

        final StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());
        final Principal user = accessor.getUser();
        if (user != null) {
//            clearSubscriptionsCommand.apply(user.getName());
        }
    }
}
