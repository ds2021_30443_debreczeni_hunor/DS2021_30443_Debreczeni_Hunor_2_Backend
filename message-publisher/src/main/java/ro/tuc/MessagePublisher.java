package ro.tuc;

import ro.tuc.messaging.MessageSender;
import ro.tuc.messaging.RabbitMQHandler;
import ro.tuc.model.MonitoredDataDTO;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class MessagePublisher {
    private static final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private static final String QUEUE_NAME = "queue";
    private static final String URI = "amqps://ikiczwkv:T17sm-m1EtbW3YbR8rBF1Mr5LWgN7aYt@cow.rmq2.cloudamqp.com/ikiczwkv";
    private static final MessageSender sender = new RabbitMQHandler(URI);

    public static void main(String[] args) throws IOException {
        final Properties props = new Properties();
        props.load(new FileInputStream("message-publisher/src/main/resources/application.properties"));
        final UUID sensorId = UUID.fromString(props.getProperty("sensorId"));

        final Stream<String> lines;
        try {
            lines = Files.lines(Path.of("message-publisher/src/main/resources/sensor.csv"));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        final Iterator<String> dataIterator = lines.iterator();
        executor.scheduleAtFixedRate(() -> {
            try {
                if (!dataIterator.hasNext()) {
                    executor.shutdown();
                    sender.close();
                    return;
                }

                final MonitoredDataDTO sensorData = new MonitoredDataDTO(
                        System.currentTimeMillis(),
                        sensorId,
                        Double.parseDouble(dataIterator.next())
                );

                System.out.println("Sent message with " + sensorData);
                sender.send(QUEUE_NAME, sensorData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, 0, 10, TimeUnit.MINUTES);
    }
}
