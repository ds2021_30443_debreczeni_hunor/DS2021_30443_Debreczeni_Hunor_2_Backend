package ro.tuc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@Data
@AllArgsConstructor
public class MonitoredDataDTO {
    private Long timestamp;
    private UUID sensor_id;
    private Double measurement_value;
}