package ro.tuc.messaging;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class RabbitMQHandler implements MessageSender {
    private Connection connection;
    private Channel channel;

    public RabbitMQHandler(String uri) {
        initChannel(uri);
    }

    private void initChannel(String uri) {
        ConnectionFactory factory = new ConnectionFactory();
        try {
            factory.setUri(uri);
            factory.setRequestedHeartbeat(30);
            factory.setConnectionTimeout(30000);

            connection = factory.newConnection();
            channel = connection.createChannel();
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException | TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public <T> void send(String queueName, T obj) throws IOException {
        channel.basicPublish("", queueName, null, new Gson().toJson(obj).getBytes());
    }

    @Override
    public void close() throws Exception {
        channel.close();
        connection.close();
    }
}
